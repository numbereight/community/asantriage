from typing import List
import re
import sys

# Match the file.cpp:line:col at the end of the message
p = re.compile(".*.([a-z]+):\d+:\d+$")

def group_stacktrace(stack_trace: List[str]):
    for line in stack_trace:
        if p.match(line):
            return line[-1 * min(100, len(line)):]
    # If no symbols were enabled, group on the entire message
    return "|".join(stack_trace)

groups_count = {}
num_direct = 0
num_indirect = 0
def output_stacktrace(stack_trace: List[str]):
    if stack_trace[0].startswith("Direct"):
        leak_type = "DIRECT"
    elif stack_trace[0].startswith("Indirect"):
        leak_type = "INDIRECT"
    else:
        leak_type = "UNKNOWN"

    group = group_stacktrace(stack_trace)
    if group in groups_count:
        groups_count[group][0] += 1
    else:
        if leak_type == "DIRECT":
            global num_direct
            num_direct += 1
        elif leak_type == "INDIRECT":
            global num_indirect
            num_indirect += 1
        groups_count[group] = [1, "\n".join(stack_trace), leak_type]

has_started_asan_output = False
with open(sys.argv[1], "r") as f:
    current_stack_trace = []
    for line in f:
        if not has_started_asan_output and "==ERROR: LeakSanitizer:" in line:
            has_started_asan_output = True

        if has_started_asan_output:
            # Capture into stack traces

            if line == "":
                continue
            should_dump = False
            if line.startswith("Direct leak of"):
                should_dump = True
            if line.startswith("Indirect leak of"):
                should_dump = True
            if line.startswith("SUMMARY:"):
                should_dump = True

            # Dump current stack trace
            if len(current_stack_trace) > 0 and should_dump:
                output_stacktrace(current_stack_trace)
                current_stack_trace = []
            cleaned_line = line.strip()
            if cleaned_line != "":
                current_stack_trace.append(cleaned_line)

# Output each group once
for group, val in groups_count.items():
    count = val[0]
    stack_trace = val[1]
    leak_type = val[2]
    print("----------------------")
    print(stack_trace)
    print("")
    print("Leak type: " + leak_type)
    print("Count: " + str(count))
print("")
print("Num Direct: " + str(num_direct))
print("Num Indirect: " + str(num_indirect))
